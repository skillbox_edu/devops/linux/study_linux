#!/bin/bash

function get_extension() {
  extension=$(echo "$1" | sed 's/.*\.//')
  echo $extension
}

function check_unzip_is_installed() {
  (apt list --installed | grep -P "^unzip(?=/)") &> /dev/null
  echo $?
}

function main() {
  if [ $# -ne 1 ]; then
    echo "expected one argument, but got $#"
    exit 1
  fi

  extension=$(get_extension $1)
  if [ $extension = $1 ]; then
    extension=''
  fi

  if [ -z $extension ]; then
    echo "can't unpack file: $1. Expected file extension: .gz, .bz2, .lzma, .zip"
    exit 1
  fi

  case "$extension" in
    "gz") gunzip "$1" ;;
    "bz2") bunzip2 "$1" ;;
    "lzma") unlzma "$1" ;;
    "zip")
      if [ $(check_unzip_is_installed) -eq 0 ]; then
        (unzip "$1" && rm -f "$1") >> /dev/null
      else
        echo "unzip not installed"
        exit 1
      fi ;;
    *) echo "unknown extension $extension"; exit 1 ;;
  esac
}

main $@
