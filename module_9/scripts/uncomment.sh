#!/bin/bash

OUTPUT_FILE_SUFFIX="uncomment"

function remove_comments() {
  sed 's/^#.*//' "$1" 1> "$2"
}

function main () {
  if [ $# -ne 1 ]; then
    echo "expected one argument, but got $#"
    exit 1
  elif ! [ -f "$1" ]; then
    echo "file '$1' doesn't exists or not file"
    exit 1
  fi

  output_file_path="$1_$OUTPUT_FILE_SUFFIX"
  remove_comments "$1" "$output_file_path"

  echo "file '$1' without comments: $output_file_path"
}

main $@
