# Домашнее задание по модулю "Написание скриптов на bash. Заключение" курса "Старт в DevOps"

## Что нужно сделать
1. Напишите скрипт для распаковки заархивированных файлов. Скрипт должен обрабатывать архивы в формате `.gz`, `.bz2`, `.lzma`, `.zip`.
   
   Не забудьте, что программу `unzip` вам потребуется установить. В скрипте нужно проверять, установлена ли программа, если нет — выдавать сообщение пользователю.   

   Скрипт должен принимать в качестве аргумента имя файла и самостоятельно решать, какую программу для распаковки необходимо использовать.

   Пришлите текст скрипта, скриншоты в момент работы (покажите, что файлы действительно распаковываются).

1. Как мы уже отмечали, программа `rm` не даёт права на ошибку и не поддерживает механизма на случай, если вы ошиблись или передумали удалять файл. Давайте это исправим. Напишите скрипт под названием `delete`, который будет вместо удаления файла сжимать его (можно выбрать алгоритм gzip или lzma) и перемещать в папку `/home/username/TRASH`.

   Каждый раз при запуске скрипт должен просматривать папку `TRASH` и удалять из неё файлы старше 48 часов.

   Сделайте так, чтобы наш скрипт `delete` поддерживал не только удаление файлов, но и удаление директорий.

   Пришлите текст скрипта, скриншоты во время работы (покажите, что файлы действительно не удаляются, а перемещаются в папку `TRASH`).

1. Напишите скрипт, который будет принимать в качестве аргумента имя скрипта и удалять из него все комментарии. Сам скрипт не нужно редактировать, результат работы сохраните в другой файл и выведите пользователю имя нового файла. 

   Не забудьте, что шэбэнг тоже начинается с решётки, но он должен сохраниться.

   Пришлите текст скрипта, а также скриншоты во время работы.

## Порядок выполнения
1. Напишите скрипт для распаковки заархивированных файлов. Скрипт должен обрабатывать архивы в формате `.gz`, `.bz2`, `.lzma`, `.zip`.
   
   ```shell
   chmod +x unzip
   ```
   
   Скрипт: [unzip.sh](scripts/unzip.sh)

   Создать сжатые файлы:
   ```shell
   touch file
   echo "hello" > file
   gzip file > file.gz
   bzip2 file > file.bz2
   lzma file > file.lzma
   zip file.zip file
   rm file
   ```
   ![task1-1](docs/task1-1.png)
   
   Распаковка gzip:
   ```shell
   ./unzip file.gz; ls file; ls file.gz
   ```
   ![task1-2](docs/task1-2.png)
   
   Распаковка bzip2:
   ```shell
   ./unzip file.bz2; ls file; ls file.bz2
   ```
   ![task1-3](docs/task1-3.png)
   
   Распаковка lzma:
   ```shell
   ./unzip file.lzma; ls file; ls file.lzma
   ```
   ![task1-4](docs/task1-4.png)

   Распаковка zip:
   ```shell
   ./unzip file.zip; ls file; ls file.zip
   ```
   ![task1-5](docs/task1-5.png)

   Вызов без аргументов:
   ```shell
   ./unzip
   ```
   ![task1-6](docs/task1-6.png)

   Вызов на файле без известного расширения:
   ```shell
   ./unzip file
   ```
   ![task1-7](docs/task1-7.png)

   Вызов на файле с неподдерживаемым расширением:
   ```shell
   ./unzip file.7z
   ```
   ![task1-8](docs/task1-8.png)

   Когда `unzip` не установлен:
   ```shell
   ./unzip file.zip
   ```
   ![task1-9](docs/task1-9.png)

1. Напишите скрипт под названием `delete`, который будет вместо удаления файла сжимать его (можно выбрать алгоритм gzip или lzma) и перемещать в папку `/home/username/TRASH`.

   ```shell
   chmod +x delete
   ```
   
   Скрипт: [delete.sh](scripts/delete.sh)

   Создать файл и директорию, которые будем удалять:
   ```shell
   touch file; echo "hello" > file
   mkdir devops devops/dir1 devops/dir2
   touch devops/file1 devops/file2 devops/dir1/file3 devops/dir2/file4
   ls -R .
   ```
   ![task2-1](docs/task2-1.png)
   
   Вызов без аргументов:
   ```shell
   ./delete
   ```
   ![task2-2](docs/task2-2.png)
   
   Вызов с несуществующим файлом аргументов:
   ```shell
   ./delete not_existed_file
   ```
   ![task2-3](docs/task2-3.png)
   
   Удаление файла и директории:
   ```shell
   ./delete file devops; ls .; ls TRASH
   ```
   ![task2-4](docs/task2-4.png)

   Содержимое архивов:
   ```shell
   tar -tf TRASH/file.tar.gz; echo "---"; tar -tf TRASH/devops.tar.gz
   ```
   ![task2-5](docs/task2-5.png)
   
   Восстановление файла:
   ```shell
   tar -xf TRASH/file.tar.gz; cat file
   ```
   ![task2-6](docs/task2-6.png)

1. Напишите скрипт, который будет принимать в качестве аргумента имя скрипта и удалять из него все комментарии. Сам скрипт не нужно редактировать, результат работы сохраните в другой файл и выведите пользователю имя нового файла.

   ```shell
   chmod +x uncomment
   ```
   
   Скрипт: [uncomment.sh](scripts/uncomment.sh)
   
   Тестовый скрипт с комментариями: [script_with_comments.sh](scripts/script_with_comments.sh)

   Вызов без с непрвильным количеством аргументов:
   ```shell
   ./uncomment
   ./uncomment file1 file2
   ```
   ![task3-1](docs/task3-1.png)
   ![task3-2](docs/task3-2.png)
   
   Вызов с несуществующим файлом:
   ```shell
   ./uncomment ~/file
   ```
   ![task3-3](docs/task3-3.png)

   Вызов для замены комментариев:
   ```shell
   cat script_with_comments
   ./uncomment script_with_comments; cat script_with_comments_uncomment
   ```
   ![task3-4](docs/task3-4.png)

   ![task3-5](docs/task3-5.png)
