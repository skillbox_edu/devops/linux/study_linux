#!/bin/bash
ERROR=1
SUCCESS=0

if [ $# -ne 2 ]
then
  echo "expected 2 arguments, but got $#"
  exit $ERROR
fi

if [ ! -f "$1" ]
then
  echo "'$1' not exists or not file"
  exit $ERROR
fi

if cp "$1" "$2" && rm -f "$1"
then
  echo "Seccessfuly renamed '$1' to '$2'"
  exit $SUCCESS
else
  echo "Something went wrong"
  exit $ERROR
fi
