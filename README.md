# Домашние задания курса "Старт в DevOps: системное администрирование для начинающих"

1. Введение в Linux
1. [Linux: первые шаги](module_2/README.md)
1. [Пользователи и файлы. Знакомство с Vim](module_3/README.md)
1. [Подготовка к написанию скриптов на bash](module_4/README.md)
1. [Написание скриптов на bash](module_5/README.md)
1. [Написание скриптов на bash. Продолжение](module_6/README.md)
1. [Написание скриптов на bash. Циклы](module_7/README.md)
1. [Написание скриптов на bash. Циклы: продолжение](module_8/README.md)
1. [Написание скриптов на bash. Заключение](module_9/README.md)
1. [World Wide Web](module_10/README.md)
1. [World Wide Web](module_10/README.md)
1. [Системы инициализации](module_11/README.md)
1. [Криптография. HTTPS](module_12/README.md)
1. [Работа с устройствами в Linux](module_13/README.md)
