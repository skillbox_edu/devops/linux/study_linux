# Домашнее задание по модулю "Работа с устройствами в Linux" курса "Старт в DevOps"

## Что нужно сделать
1. Попрактиковаться в монтировании устройств
   1. [Скачайте](https://releases.ubuntu.com/20.04.2/ubuntu-20.04.2-live-server-amd64.iso?_ga=2.47396641.723314521.1620383292-70101866.1620383292) образ диска для установки Ubuntu.
   1. Примонтируйте образ к операционной системе.
   
   Пришлите скриншоты команд через форму для сдачи домашнего задания.

1. Как было показано на уроке, подключите новый жёсткий диск к виртуальной машине, разметьте его и настройте автоматическое монтирование при загрузке.

   Пришлите скриншоты конфигурационных файлов и процесса разметки диска через форму для сдачи домашнего задания.

1. Самостоятельно найдите информацию для ответа на вопросы:

   1. Какие файловые системы, помимо ext4, распространены в Linux сейчас?
   1. Какие у них есть преимущества и интересные особенности? 
   1. Какие файловые системы считаются устаревшими и не используются?

## Порядок выполнения
1. Скачайте образ диска для установки Ubuntu. Примонтируйте образ к операционной системе.
   
   ```shell
   sudo mkdir /media/module_13_ubuntu_iso
   sudo mount ~/ubuntu-20.04.2-live-server-amd64.iso /media/module_13_ubuntu_iso -o loop
   mount | grep module_13_ubuntu_iso
   df -h | grep module_13_ubuntu_iso
   ```
   ![task1-1](docs/task1-1.png)
