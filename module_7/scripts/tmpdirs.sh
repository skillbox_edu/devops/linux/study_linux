#!/bin/bash
declare -r SLEEP_TIME=$(expr 7 * 60)  # in minutes
i=0

while [ $i -lt 7 ]
do
  mkdir /tmp/directory-$(date +"%Y%m%d_%H%M")
  sleep $SLEEP_TIME

  i=$(expr $i + 1)
done
