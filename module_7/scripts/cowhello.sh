#!/bin/bash
while true
do
  echo "Who do you want advice from?"

  cat << options
  bunny
  tux
  daemon
  kitty
  vader-koala
options

echo
read -p "Make you choice or type 'quit' for exit: " choice

if [ $choice = 'quit' ]
then
  echo "Bye!"
  exit 0
fi

fortune | cowsay -f $choice
echo

done
