#!/bin/bash
num=0
while [ $num -lt 10 ]
do
  if [ $(expr $num % 2) -eq 0 ]
  then
    echo $num
  fi
  num=$(expr $num + 1)
done
