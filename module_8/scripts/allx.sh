#!/bin/bash

print_executable_files() {
  for obj in $(ls -d "$1/"*)
  do
    if [ -d obj ]
    then
      print_executable_files $obj
    elif [ -x obj ]
    then
      echo $obj
    fi
  done
}

print_executable_files "/usr"
