#!/bin/bash
USER_DIR=/home/vagrant
OUT_DIR_NAME=devops
OUT_DIR=$USER_DIR/$OUT_DIR_NAME

mkdir -p $OUT_DIR

tar -czf $OUT_DIR/backup-$(date +"%Y-%m-%d").tar.gz --exclude $OUT_DIR_NAME $(ls $USER_DIR) &> /dev/null
