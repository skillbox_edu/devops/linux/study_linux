#!/bin/bash
for obj in $(ls -d "/etc/"*)
do
  if [ -f $obj ]
  then
    echo $(wc -l $obj)
  fi
done
