#!/bin/bash
if [ $# -ne 1 ]
then
  echo "expected 1 argument, but got $#"
  exit 1
fi

ping -c 4 $1 | tail -3
