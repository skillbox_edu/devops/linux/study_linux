#!/bin/bash
read -p "enter first operand: " x
read -p "enter second operand: " y
read -p "enter operator: " operator

if [ $operator = "/" ] && [ $y -eq 0]
then
  echo "zero division error"
  exit 1
fi

case $operator in
  "+" ) let res="$x + $y" ;;
  "-" ) let res="$x - $y" ;;
  "*" ) let res="$x * $y" ;;
  "/" ) let res="$x / $y" ;;
  "**" ) let res="$x ** $y" ;;
  "%" ) let res="$x % $y" ;;
  * ) echo "unknown operator: $operator"; exit 1
esac

echo "$x $operator $y = $res"
