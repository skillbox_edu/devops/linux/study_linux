#!/bin/bash

TRASH_DIR="$HOME/TRASH"

function exists() {
  if [ -e "$1" ]; then
    echo 1
  else
    echo 0
  fi
}

function replace_slash() {
  res=$(echo "$1" | sed 's/\//-/g')
  echo $res
}

function delete() {
  mkdir -p "$TRASH_DIR"
  tar -czf "$TRASH_DIR/$(replace_slash "$1").tar.gz" "$1"
  if [ -f "$1" ]; then
    rm -f "$1"
  elif [ -d "$1" ]; then
    rm -rf "$1"
  fi
}

function remove_old_trash() {
 if ! [ -e $TRASH_DIR ]; then
   return
 fi

 for item in $(find $TRASH_DIR -mtime +2); do
    rm -f "$item"
  done
}

function main() {
  if [ $# -le 0 ]; then
    echo "expected arguments, but got $#"
    exit 1
  fi

  remove_old_trash

  for item in $@; do
    if ! [ $(exists "$item") -eq 1 ]; then
      echo "file '$item' doesn't exists"
      continue
    fi

    if [ -L "$item" ]; then
      # если файл является символической ссылкой,
      # то удаляем только ссылку и показываем,
      # где расположен оригинальный файл
      original_file=$(readlink "$item")
      unlink "$item"
      echo "symlink '$item' was deleted. Original file: $original_file"
    else
      delete "$item"
    fi
  done
}

main $@
