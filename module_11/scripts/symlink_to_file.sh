#!/bin/bash
if [ $# -eq 0 ]; then
  echo "expected arguments, but got $#"
  exit 1
fi

for item in $@
do
  if [ -L "$item" ]; then
    symlink_content=$(readlink "$item")
    unlink "$item"
    touch "$item"
    echo $symlink_content > "$item"
  fi
done

