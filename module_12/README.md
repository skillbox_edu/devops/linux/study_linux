# Домашнее задание по модулю "Криптография. HTTPS" курса "Старт в DevOps"

## Что нужно сделать
1. Представьте, что вам нужно безопасно передать кому-то несколько текстовых файлов. Запакуйте эти файлы в tar.gz архив и зашифруйте его с помощью симметричного шифрования. 

   Пришлите:
      * скриншоты команд, которые вы вводили для архивирования, шифрования и расшифровывания;
      * скриншоты с успешно распакованными и расшифрованными текстовыми файлами.

1. Письменно ответьте на вопрос: изменится ли хеш текстового файла, если добавить в него пустую строку? Напомню, что для вычисления хеш-суммы можно использовать команду `md5sum`.

   Пришлите ответ на вопрос и скриншоты с хеш-суммой файла до и после изменений через форму для сдачи домашнего задания.

1. Сгенерируйте самоподписанный сертификат и настройте Nginx на работу по HTTPS для тестовой веб-странички, как было показано на уроке.

   Пришлите:
      * скриншот конфигурационного файла nginx,
      * скриншот браузера с открытой по HTTPS веб-страницей.

1. Воспользовавшись инструкцией, сгенерируйте ключевую пару «открытый и закрытый ключ». При помощи открытого ключа зашифруйте файл. Затем расшифруйте его при помощи приватного ключа. Убедитесь, что зашифрованный файл нельзя прочитать как текстовый, а расшифрованный файл совпадает с исходным.

   Инструкция:

   1. Генерируем приватный ключ
      ```shell
      openssl genpkey -algorithm RSA -out private.key -pkeyopt rsa_keygen_bits:8192
      ```
   1. Извлекаем из приватного ключа публичный ключ
      ```shell
      openssl rsa -in private.key -pubout -out public.key
   1. Шифруем файл
      ```shell
      openssl rsautl -encrypt -pubin -inkey публичный_ключ.key -in файл_с_открытым_текстом.txt -out зашифрованный_файл.txt.enc
   1. Расшифровываем файл
      ```shell
      openssl rsautl -decrypt -inkey приватный_ключ.key -in зашифрованный_файл.txt.enc  -out файл_с_открытым_текстом.txt.new
      ```

   Пришлите:
      * скриншот с использованными командами для шифрования и расшифровки,
      * скриншоты с успешно расшифрованным текстовым файлом.

## Порядок выполнения
1. Запакуйте эти файлы в tar.gz архив и зашифруйте его с помощью симметричного шифрования.

   ```shell
   touch file; echo "hello" > file
   tar -czf arch.tar.gz file
   rm -f file
   ls arch.tar.gz
   openssl enc -aes-256-cbc -in arch.tar.gz -out arch.tar.gz.enc
   ls arch.tar.gz.enc
   rm -f arch.tar.gz
   openssl enc -aes-256-cbc -d -in arch.tar.gz.enc -out arch.tar.gz
   tar -tf arch.tar.gz
   tar -xf arch.tar.gz
   cat file
   ls -l
   ```
   ![task1-1](docs/task1-1.png)

1. Письменно ответьте на вопрос: изменится ли хеш текстового файла, если добавить в него пустую строку?

   Хэш изменится, потому что изменится содержимое файла. На этом принципе Git работает.
   ```shell
   touch file; echo "hello" > file
   md5sum file
   echo $'\n' >> file
   md5sum file
   ```
   ![task2-1](docs/task2-1.png)

1. Сгенерируйте самоподписанный сертификат и настройте Nginx на работу по HTTPS для тестовой веб-странички

   Генерируем самоподписанный сертификат:
   ```shell
   openssl genrsa -out server.key 4096
   openssl req -new -key server.key -out server.csr
   openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
   ```
   
   Веб-страница: [hello-world.html](conf/hello-world.html)

   Конфигурация `nginx`: [default-nginx.conf](conf/default-nginx.conf)

   Применение конфигурации:
   ```shell
   sudo mkdir -p /etc/nginx/ssl
   sudo cp server.key server.crt /etc/nginx/ssl/
   sudo cp hello-world.html /var/www/html/
   sudo cp default-nginx.conf /etc/nginx/sites-enabled/default
   sudo systemctl reload nginx
   ```

   Запрос страницы через `curl`:
   ```shell
   curl https://127.0.0.1
   curl -k https://127.0.0.1
   ```
   ![task3-1](docs/task3-1.png)

   Веб-страница, открытая в Links:
   ![task3-2](docs/task3-2.png)
   
1. Воспользовавшись инструкцией, сгенерируйте ключевую пару «открытый и закрытый ключ». При помощи открытого ключа зашифруйте файл. Затем расшифруйте его при помощи приватного ключа. Убедитесь, что зашифрованный файл нельзя прочитать как текстовый, а расшифрованный файл совпадает с исходным.

   ```shell
   # Создаем файл
   touch file; echo "hello" > file
   # Генерируем приватный ключ
   openssl genpkey -algorithm RSA -out private.key -pkeyopt rsa_keygen_bits:8192
   # Извлекаем из приватного ключа публичный ключ
   openssl rsa -in private.key -pubout -out public.key
   # Шифруем файл
   openssl rsautl -encrypt -pubin -inkey public.key -in file -out file.enc
   # Расшифровываем файл
   openssl rsautl -decrypt -inkey private.key -in file.enc -out file_new
   # Проверяем, что зашифрованый файл уже не обычный текстовый
   cat file.enc | grep "hello"
   # Проверяем, что исходный файл и расшифрованный совпадают
   test $(cat file) = $(cat file_new); echo $?
   # Читаем расшифрованный файл
   cat file_new
   ```
   ![task4-1](docs/task4-1.png)
